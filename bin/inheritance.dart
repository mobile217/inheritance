
class ninokuni{
  var name;
  var career;
  var sex;
  var race;
  var weapon;

  void ninokunis(String name, String sex, String race, String weapon){
    print("Ni no Kuni: Cross Worlds Created");
    this.name = name;
    this.sex = sex;
    this.race = race;
    this.weapon = weapon;
  }

  getName(){
    return name;
  }

  getSex(){
    return sex;
  }

  getRace(){
    return race;
  }

  getWeapon(){
    return weapon;
  }

  void style(){
    print("Ni no kuni Style");
  }
}

class closeattack extends ninokuni{
  var name = "Swordman";
  var sex = "male";
  var race = "human";
  var weapon = "one handed sword";
  void style(){
    print("Close Attack: " + name
    + "\nNi no kuni Style: " + weapon
    + "\nSex: " + sex
    + "\nRace: " + race);
    //super.style();
  }
}

class rangedattack extends ninokuni{
  var name = "Rogue";
  var sex = "male";
  var race = "half human half grimalkin";
  var weapon = "archer";
  void style(){
    print("Ranged Attack: " + name
    + "\nNi no kuni Style: " + weapon
    + "\nSex: " + sex
    + "\nRace: " + race);
    //super.style();
  }
}

class tank extends ninokuni{
  var name = "Destroyer";
  var sex = "male";
  var race = "human";
  var weapon = "hammer";
  void style(){
    print("Tank: " + name
    + "\nNi no kuni Style: " + weapon
    + "\nSex: " + sex
    + "\nRace: " + race);
    //super.style();
  }
}

class healer extends ninokuni{
  var name = "Engineer";
  var sex = "female";
  var race = "human";
  var weapon = "rifle";
  void style(){
    print("Healer: " + name
    + "\nNi no kuni Style: " + weapon
    + "\nSex: " + sex
    + "\nRace: " + race);
    //super.style();
  }
}

class magician extends ninokuni{
  var name = "Witch";
  var sex = "female";
  var race = "half human half grimalkin";
  var weapon = "spear";
  void style(){
    print("Magician: " + name
    + "\nNi no kuni Style: " + weapon
    + "\nSex: " + sex
    + "\nRace: " + race);
    //super.style();
  }
}

void main() {
    closeattack ca1 = closeattack();
    ca1.style();
    print("----------------------------------");
    rangedattack ra1 = rangedattack();
    ra1.style();
    print("----------------------------------");
    tank t1 = tank();
    t1.style();
    print("----------------------------------");
    healer h1 = healer();
    h1.style();
    print("----------------------------------");
    magician m1 = magician();
    m1.style();
    print("----------------------------------");
  }